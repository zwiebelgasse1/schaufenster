const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);



app.get('/', (req, res) => {
    //res.send('<h1>Hello world</h1>');
    res.sendFile(__dirname + '/client.html');
});

app.use(express.static(__dirname + '/Astray'));
app.use(express.static(__dirname + '/game'));
app.get('/game', (req, res) => {
    //res.sendFile(__dirname + '/Astray/index.html');
    res.sendFile(__dirname + '/game/index.html');
});

io.on('connection', (socket) => {


    //Set room for user when connection is made
    socket.on('setChannel', function (data) {
        socket.join(data.channelName);
        console.log('_set Channel:', data.channelName);

        //socket.broadcast.to(data.channelName).emit('gameChanges', 'wuff');
        //io.emit('gameChanges', 'wuff2');
    });

    //When iOS device moves send data to browser
    //Multiple browsers can be listening to same device
    socket.on('spaceChange', function (data) {

        //socket.broadcast.to(data.channelName).emit("gameChanges", data);
        io.emit('gameChanges', data);
        console.log('_set spaceChange', data);
    });

    socket.on('spaceEnd', function (data) {

        //socket.broadcast.to(data.channelName).emit("gameChanges", data);
        io.emit('gameChangesEnd', data);
        console.log('_set gameChangesEnd', data);
    });




    socket.on('createPlayer', function (data) {
        io.emit('createPlayer', data);
        console.log('_set createPlayer', data);
    });

    socket.on('gameOver', function () {
        io.emit('gameOver');
        console.log('_set gameOver');
    });








});

var port = process.env.PORT || 5000;

server.listen(port, () => {
    console.log('listening on *:5000', process.env.PORT || 5000);
});